# Contribution
Standards for contibuting in this project.
- The coding Standard should follow
 [PEP8](https://www.python.org/dev/peps/pep-0008/) Standard.
- Do not send merge request before testing your code.
- Write some good test to test your code or seek helps from community.
- Do not hesitate to ask for code review from community you might get some
good suggestions.
