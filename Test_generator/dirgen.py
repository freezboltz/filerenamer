#!/bin/env python

import os
import shutil
from pathlib import Path
from termcolor import colored
CORE_PROJECT_DIR = Path(__file__).resolve(strict=True).parent.parent

parent_dir = str(CORE_PROJECT_DIR / "Testground")
isDir = os.path.isdir(parent_dir)


def main() -> int:
    if isDir:
        shutil.rmtree(parent_dir)

    dirgen_path = os.path.join(parent_dir, "DIRGEN")

    dirList = ['Dir 1', 'Dir 2', 'Dir 3', 'Dir 4',
               'Dir 5', 'Dir 6', 'Dir 7', 'Dir 8']
    fileList = ['file 1', 'file 2', 'file 3', 'file 4',
                'file 5', 'file 6', 'file 7', 'file 8']

    os.makedirs(dirgen_path)
    os.chdir(dirgen_path)

    for file in fileList:
        with open(f'{file}.txt', 'w'):
            pass
    for dir in dirList:
        os.mkdir(dir)
        os.chdir(os.path.join(dirgen_path, dir))
        for file in fileList:
            with open(f'{file}.txt', 'w'):
                pass
        os.chdir(dirgen_path)

    STATUS = colored("OK", "green")
    CONTENT = colored("Folder Structure generated", "magenta")
    print(STATUS, CONTENT)
    return 0


if __name__ == '__main__':
    exit(main())
