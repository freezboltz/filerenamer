#!/bin/env python
"""
program: file renamer
author: Anant Murmu
email: freezboltz@gmail.com
License: MIT License @copyright Anant Murmu 2023
"""

# This script is platform dependent
# Platforms supported are: unix and linux

################################################################################
# Import libraries
################################################################################
from termcolor import colored
from pathlib import Path
import string
import re
import os

################################################################################
# Variables
################################################################################
pattern_hyphen_or_comma_between_underscores = re.compile(r"_[-,]_")
pattern_space_1_or_more = re.compile(r'\s+')
pattern_underscore_2_or_more = re.compile(r"_{2,}")

_mypunch = string.punctuation
_BASE_PATH = str(os.getcwd())
_ErrorOne = colored("DirectoryError:", 'magenta')
_ErrorTwo = colored("FileNotFound:", 'magenta')
_Warning = colored("Warning:", 'yellow')
_OK = colored("OK", 'green')

################################################################################
# Definitions
################################################################################
def rename_status(oldName: str, newName: str) -> None:
    """this function prints the rename status for the input file
    """
    status = _OK
    file_or_dir = colored(file_or_directory(newName), 'cyan')
    old = colored(oldName, "red")
    new = colored(newName, "magenta")
    print(status, f"{file_or_dir}: {old} renamed to {new}")

def remove_leading_and_trailing_underscore(s: str) -> str:
    """Removes leading and trailing underscores from given string or returns none
    """
    return re.sub(r'^_+|_+$', '', s)

def info(t: str, n: str, what=None) -> int:
    """Interactive information
    """
    if what == "error":
        info = colored(n, 'red')
        print(t, info)
    elif what == "warning":
        info = colored(n, 'magenta')
        print(t, info)
    return 0


def str_or_none(v, default=None):
    """Returns value of type string or None
    """
    return default if v is None else str(v)


def file_or_directory(v, default="file"):
    """Returns string 'file' or 'directory'
    """
    return default if os.path.isfile(str(v)) else "Directory"


def symbol_to_underscore(fn: str, sb: str) -> str:
    """Returns underscore instead of supplied symbol
    """
    return "_".join(fn.split(sb))


def remove_pattern(fn: str, pat: str) -> str:
    """Removes pattern from supplied file name and returns sanitized string
    """
    return fn.replace(pat, "")


def replace_symbol(oldFileName: str) -> str:
    """function definition for replacing punctuation with underscore
    """
    oldFileName = "".join(s for s in oldFileName if s ==
                          "_" or s == "." or s == "-" or s not in _mypunch)
    oldFileName = symbol_to_underscore(oldFileName, ".")
    oldFileName = symbol_to_underscore(oldFileName, "-")
    if re.search(pattern_hyphen_or_comma_between_underscores, oldFileName):
        oldFileName = re.sub(
            pattern_hyphen_or_comma_between_underscores, "_", oldFileName.strip())
    elif re.search(pattern_hyphen_or_comma_between_underscores, oldFileName):
        oldFileName = re.sub(
            pattern_hyphen_or_comma_between_underscores, "_", oldFileName.strip())
    elif re.search(pattern_underscore_2_or_more, oldFileName):
        oldFileName = re.sub(pattern_underscore_2_or_more,
                             "_", oldFileName.strip())
    elif re.search(r"\S'", oldFileName):
        oldFileName = oldFileName.strip().replace("'", "")
    elif re.search(r'._', oldFileName):
        oldFileName = oldFileName.strip().replace("._", "_")
    else:
        pass
    return oldFileName


def replace_space(oldFileName: str) -> str:
    """function definition for removing spaces with underscore
    @params :: oldFileName: filename without extention
    """
    if re.search(pattern_space_1_or_more, oldFileName):
        oldFileName = re.sub(pattern_space_1_or_more, "_", oldFileName.strip())
        oldFileName = oldFileName.replace("_.", ".").replace("._", "_")
    else:
        pass
    return oldFileName


def underscore_cleanup(oldFileName: str) -> str:
    """This function cleans redundent underscores
    """
    if re.search(pattern_underscore_2_or_more, oldFileName):
        oldFileName = oldFileName.strip().replace("__", "_")
    elif re.search(r'_-_', oldFileName):
        oldFileName = oldFileName.strip().replace("_-_", "_")
    else:
        pass
    return oldFileName


def rename(name=None) -> int:
    """Rename default filename to user input.
    Example: "abc cat" --> abc_cat
            "door.lock.true" --> door_lock.true
    """
    if name is None:
        return 0
    if "/" in name:
        info(_ErrorOne, "'/' Detected: Not able to rename", what="error")
        return 0
    # check for file extention
    if re.search(r'[^/]+\.[^/]{3,4}$', name):
        name, extention = os.path.splitext(name)
    else:
        name = name
        extention = ""
    if extention:
        newName = underscore_cleanup(
            replace_symbol(replace_space(name))) + extention
    else:
        newName = underscore_cleanup(replace_symbol(replace_space(name)))
    print(newName)
    return 0


def rename_files_under_directory(myPath: str) -> int:
    """Rename files under directroy provided as params
    This function walk through directory and list files for processing.
    @params :: myPath --> path like object pass as string
    """
    # check if provided path is directory or not
    if not os.path.isdir(myPath):
        info(_ErrorOne, "Provided params is not a directory exiting script...", what="error")
        return 1

    fileCounter = 0
    notRenameFileCounter = 0
    dirCounter = 0
    dirlist = []
    dirname = []
    absolute_path = os.path.abspath(myPath)
    for root, _, filenames in os.walk(absolute_path):
        dirlist.append(root)
        dnList = root.split('/')
        if dnList[-1] == '':
            dirname.append(root.split('/')[-2])
        else:
            dirname.append(dnList[-1])
        os.chdir(root)
        for file in filenames:
            file_name, file_ext = os.path.splitext(file)
            name = replace_symbol(replace_space(file_name))
            new_name = f"{name}{file_ext}"

            # Check for already sanitized name
            if file_name == name:
                info(_Warning,
                     f"{file_name} not renamed.",
                     what="warning")
                notRenameFileCounter += 1
                continue
            # Check if file or directory already exist
            # prevent file overwriting
            if os.path.exists(Path(_BASE_PATH, str(new_name))):
                info(_Warning,
                     f"{new_name} already exist so not renamed.",
                     what="warning")
                continue
            try:
                os.rename(file, new_name)
                rename_status(file, new_name)
                fileCounter += 1
            except FileNotFoundError:
                info(_ErrorTwo,
                     "Provided file name or directory name doesn't exist.",
                     what="error")

    os.chdir(absolute_path)
    for dp in reversed(dirlist):
        # prevents renaming of dunder folders
        if dp.endswith("__"):
            continue
        dest_dir = ""
        # make a list of words from dp
        word_list = dp.split("/")
        if word_list[-1] == "":
            word_list[-1] = "/"
        if word_list[0] == "":
            word_list[0] = "/"

        if word_list[-1] == "/":
            fix_word = word_list[-2]
            word_list[-2] = replace_symbol(replace_space(fix_word))
            # check if directory already exists
            if word_list[-2] in dirname:
                continue
        else:
            fix_word = word_list[-1]
            word_list[-1] = replace_symbol(replace_space(fix_word))
            # check if directory already exists
            if word_list[-1] in dirname:
                continue

        for word in word_list:
            dest_dir += "/" + word

        dest_dir = Path(dest_dir)

        try:
            os.rename(dp, dest_dir)
            rename_status(dp, str(dest_dir))
            dirCounter += 1
        except:
            info(_ErrorTwo,
                 "Provided directory name doesn't exist.",
                 what="error")

    print(f"Total file processed: {fileCounter}",
          f"Total directory processed: {dirCounter}")

    return 0


def rename_local_file(file_list: list, pref=None, suf=None, rmp=None) -> int:
    """Renaming local files
    @params :: file_list --> list of files to rename
    @params :: pref --> prefix to add in filename
    @params :: suf --> suffix to add in filename
    @params :: rmp --> remove pattern from filename
    """
    count = 0
    nrCount = 0

    for name in file_list:
        if "/" in name:
            info(_ErrorOne, "'/' Detected: Not able to rename", what="error")
            continue
        if os.path.isdir(name):
            file_name = name
            file_ext = ''
            if rmp:
                file_name = remove_pattern(file_name, rmp)
        else:
            file_name, file_ext = os.path.splitext(name)
            if rmp:
                file_name = remove_pattern(file_name, rmp)
        fname = underscore_cleanup(
            replace_symbol(replace_space(file_name)))
        if suf and pref:
            prefix = underscore_cleanup(
                replace_symbol(replace_space(pref)))
            suffix = underscore_cleanup(replace_symbol(replace_space(suf)))
            new_name = f"{prefix}_{fname}_{suffix}{file_ext}"
        elif pref:
            prefix = underscore_cleanup(
                replace_symbol(replace_space(pref)))
            new_name = f"{prefix}_{fname}{file_ext}"
        elif suf:
            suffix = underscore_cleanup(replace_symbol(replace_space(suf)))
            new_name = f"{fname}_{suffix}{file_ext}"
        else:
            new_name = f"{fname}{file_ext}"

        # Check for already sanitized name
        if name == new_name:
            info(_Warning,
                 f"{name} not renamed.",
                 what="warning")
            nrCount += 1
            continue
        # Check if file or directory already exist
        # prevent file overwriting
        if os.path.exists(Path(_BASE_PATH, str(new_name))):
            info(_Warning,
                 f"{new_name} already exist so not renamed.",
                 what="warning")
            continue
        try:
            os.rename(name, new_name)
            rename_status(name, new_name)
            count += 1
        except FileNotFoundError:
            info(_ErrorTwo,
                 "Provided file name or directory name doesn't exist.",
                 what="error")

    if nrCount != 0:
        print(
            f"Total {colored(str(nrCount), 'yellow')} file or directory not renamed")
    print(f"Total {colored(str(count), 'magenta')} file or directory renamed")
    return 0
