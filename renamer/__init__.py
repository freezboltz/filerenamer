from termcolor import colored
from typing import Optional
from typing import Sequence
import platform
import argparse
from .renamer import (
        info,
        str_or_none,
        rename_local_file,
        rename_files_under_directory,
        rename,
        _Warning
        )

_ErrorThree = colored("OS ERROR:", 'magenta')

def main(argv: Optional[Sequence[str]] = None) -> int:
    """This is main function for sanitizing filename
    """
    # check of supported OS
    if platform.system() == "Windows":
        info(_ErrorThree,
             "Your OS is not supported",
             what="error")
        return 0

    # command line argument parser
    parser = argparse.ArgumentParser(
        description='Filename sanitizer for rename filenames.')
    parser.add_argument('-d', '--directory', type=str,
                        help='proviede directory for renaming file under it including parent directory')
    parser.add_argument('-n', '--name', type=str,
                        help='filename for sanitization')
    parser.add_argument('-p', '--prefix', type=str,
                        help='prefix for filenames')
    parser.add_argument('-s', '--suffix', type=str,
                        help='suffix for filenames')
    parser.add_argument('-r', '--remove', type=str, help='pattern to remove')
    parser.add_argument('-l', '--local', nargs='+', help='rename local files')
    args = parser.parse_args(argv)

    name_2_str = str_or_none(args.name)
    prefix_2_str = str_or_none(args.prefix)
    suffix_2_str = str_or_none(args.suffix)
    rmp_2_str = str_or_none(args.remove)

    if args.local:
        rename_local_file(args.local,
                          pref=prefix_2_str,
                          suf=suffix_2_str,
                          rmp=rmp_2_str)
    elif args.directory:
        rename_files_under_directory(args.directory)
    elif args.name:
        rename(name_2_str)
    else:
        info(_Warning,
             "No filename is provided exiting script doing nothing.",
             what="warning")
    return 0

__all__ = [
        'main',
        'renamer'
        ]
