#!/bin/env python
import unittest
from pathlib import Path
import sys

PROJECT_BASE_DIR = Path(__file__).resolve(strict=True).parent.parent
sys.path.insert(0, str(PROJECT_BASE_DIR))

from renamer import renamer


class TestMain(unittest.TestCase):
    """Test function in main file."""

    def test_replace_space(self):
        """Test for replace space function."""
        result1 = renamer.replace_space("this is my city.")
        result2 = renamer.replace_space("   this is my     city  .   ")
        self.assertEqual(result1, "this_is_my_city.")
        self.assertEqual(result2, "this_is_my_city.")

    def test_replace_symbol(self):
        """test for replacing symbol function."""
        result1 = renamer.replace_symbol("Kenny's  Garage")
        result2 = renamer.replace_symbol("Ken's  O'slan")
        result3 = renamer.replace_symbol("KensttpOslan")
        result4 = renamer.replace_symbol("Ken_-_sttpOslan")
        result5 = renamer.replace_symbol("Kens_,_ttpOslan")
        result6 = renamer.replace_symbol("Kens___parking")
        self.assertEqual(result1, "Kennys  Garage")
        self.assertEqual(result2, "Kens  Oslan")
        self.assertEqual(result3, "KensttpOslan")
        self.assertEqual(result4, "Ken_sttpOslan")
        self.assertEqual(result5, "Kens_ttpOslan")
        self.assertEqual(result6, "Kens_parking")

    def test_replace_leading_and_trailing_underscores(self):
        """test for removing leading and trailing underscores."""
        result1 = renamer.remove_leading_and_trailing_underscore("__TRD__")
        result2 = renamer.remove_leading_and_trailing_underscore("_._TRD_._")
        result3 = renamer.remove_leading_and_trailing_underscore("_-_TRD_-_")
        result4 = renamer.remove_leading_and_trailing_underscore("_-_TRD_._")
        result5 = renamer.remove_leading_and_trailing_underscore("_._TRD_-_")
        self.assertEqual(result1, "TRD")
        self.assertEqual(result2, "._TRD_.")
        self.assertEqual(result3, "-_TRD_-")
        self.assertEqual(result4, "-_TRD_.")
        self.assertEqual(result5, "._TRD_-")



if __name__ == "__main__":
    unittest.main()
